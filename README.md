Utils validators
================

This is simple Java library for validating some ... things.
Its aim is to be as simple as possible and be not dependent on external libraries.
At this moment there are methods for validating:

- polish PESEL number
- polish NIP number
- polish REGON number
(for all above it is using official algorithms)
