package net.wojciech.utils.validators;

/**
 * The Constants.
 */
public class Constants {

    /**
     * The constant WEIGHT_ZERO.
     */
    public static final int WEIGHT_ZERO = 0;

    /**
     * The constant WEIGHT_ONE.
     */
    public static final int WEIGHT_ONE = 1;

    /**
     * The constant WEIGHT_TWO.
     */
    public static final int WEIGHT_TWO = 2;

    /**
     * The constant WEIGHT_THREE.
     */
    public static final int WEIGHT_THREE = 3;

    /**
     * The constant WEIGHT_FOUR.
     */
    public static final int WEIGHT_FOUR = 4;

    /**
     * The constant WEIGHT_FIVE.
     */
    public static final int WEIGHT_FIVE = 5;

    /**
     * The constant WEIGHT_SIX.
     */
    public static final int WEIGHT_SIX = 6;

    /**
     * The constant WEIGHT_SEVEN.
     */
    public static final int WEIGHT_SEVEN = 7;

    /**
     * The constant WEIGHT_EIGHT.
     */
    public static final int WEIGHT_EIGHT = 8;

    /**
     * The constant WEIGHT_NINE.
     */
    public static final int WEIGHT_NINE = 9;

    /**
     * Instantiates a new Constants.
     */
    private Constants() {
        // empty
    }
}
