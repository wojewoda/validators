package net.wojciech.utils.validators;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Validator for polish tax id - NIP number.
 */
public class NipValidator {

    /**
     * The constant NIP_IS_INVALID.
     */
    private static final String NIP_IS_INVALID = "NIP is invalid";

    /**
     * The constant NIP_LENGHT.
     */
    private static final int NIP_LENGHT = 10;

    /**
     * The constant FIELD_SEPARATOR.
     */
    private static final String FIELD_SEPARATOR = ";";

    /**
     * The constant TAXDEPARTMENTS_FILE.
     */
    private static final String TAXDEPARTMENTS_FILE = "TaxDepartments.csv";

    /**
     * The constant TAX_DEPARTMENTS_DATA_DATE.
     */
    private static final String TAX_DEPARTMENTS_DATA_DATE = "2014-09-27";

    /**
     * Default constructor. Utility class - avoid instantiation.
     */
    private NipValidator() {
        // empty
    }

    /**
     * Returns date for the data file with tax departments. You can see how 'fresh' is the file.
     *
     * @return String representing date.
     */
    public static String getTaxdepartmentsDataDate() {
        return TAX_DEPARTMENTS_DATA_DATE;
    }

    /**
     * Checks if NIP number is valid.
     *
     * @param nip the nip number
     * @return true if NIP is valid; false otherwise.
     */
    public static Boolean isValid(String nip) {

        if (nip == null || nip.length() != NIP_LENGHT) {
            return Boolean.FALSE;
        }

        Integer[] nipDigits = new Integer[10];

        int i = 0;
        for (Character element : nip.toCharArray()) {
            if (!Character.isDigit(element)) {
                return Boolean.FALSE;
            } else {
                nipDigits[i] = Integer.parseInt(element.toString());
                i++;
            }
        }

        Integer checksum = Integer.parseInt(nip.substring(nip.length() - 1));
        Integer computedChecksum = Constants.WEIGHT_SIX * nipDigits[0] + Constants.WEIGHT_FIVE * nipDigits[1]
                + Constants.WEIGHT_SEVEN * nipDigits[2] + Constants.WEIGHT_TWO * nipDigits[3] + Constants.WEIGHT_THREE * nipDigits[4]
                + Constants.WEIGHT_FOUR * nipDigits[5] + Constants.WEIGHT_FIVE * nipDigits[6] + Constants.WEIGHT_SIX * nipDigits[7]
                + Constants.WEIGHT_SEVEN * nipDigits[8];
        computedChecksum = computedChecksum % 11;

        return computedChecksum.equals(checksum) ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * Gets tax department name from the data file.
     *
     * @param nip the nip number
     * @return the tax department name
     * @throws IOException the io exception
     */
    public static String getTaxDepartmentName(String nip) throws IOException {

        if (!isValid(nip)) {
            throw new IllegalArgumentException(NIP_IS_INVALID);
        }

        String taxDepartmentCode = nip.substring(0, 3);
        InputStream inputStream = NipValidator.class.getClassLoader().getResourceAsStream(TAXDEPARTMENTS_FILE);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] lineValues = line.split(FIELD_SEPARATOR);
            if ((lineValues.length == 2) && (lineValues[0].equals(taxDepartmentCode))) {
                return lineValues[1];
            }
        }

        return "";
    }
}
