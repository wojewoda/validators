package net.wojciech.utils.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Validator for polish personal id - PESEL number.
 */
public class PeselValidator {

    /**
     * The constant GENDER_CHAR_POSITION. Position for number indicating gender.
     */
    private static final int GENDER_CHAR_POSITION = 9;

    /**
     * The constant PESEL_LENGHT.
     */
    private static final int PESEL_LENGHT = 11;

    /**
     * The constant PESEL_IS_INVALID.
     */
    private static final String PESEL_IS_INVALID = "PESEL is invalid";

    /**
     * Default constructor. Utility class, prevent instantiations.
     */
    private PeselValidator() {
        // empty
    }

    /**
     * Checks if PESEL is valid.
     *
     * @param pesel the PESEL number
     * @return true if PESEL is valid; false otherwise.
     */
    public static Boolean isValid(String pesel) {

        if (pesel == null || pesel.length() != PESEL_LENGHT) {
            return Boolean.FALSE;
        }

        Integer[] peselDigits = new Integer[11];

        int i = 0;
        for (Character element : pesel.toCharArray()) {
            if (!Character.isDigit(element)) {
                return Boolean.FALSE;
            } else {
                peselDigits[i] = Integer.parseInt(element.toString());
                i++;
            }
        }

        if (!isDateValid(pesel)) {
            return Boolean.FALSE;
        }

        Integer checksum = Integer.parseInt(pesel.substring(pesel.length() - 1));
        Integer computedChecksum = Constants.WEIGHT_ONE * peselDigits[0] + Constants.WEIGHT_THREE * peselDigits[1]
                + Constants.WEIGHT_SEVEN * peselDigits[2] + Constants.WEIGHT_NINE * peselDigits[3] + Constants.WEIGHT_ONE * peselDigits[4]
                + Constants.WEIGHT_THREE * peselDigits[5] + Constants.WEIGHT_SEVEN * peselDigits[6] + Constants.WEIGHT_NINE * peselDigits[7]
                + Constants.WEIGHT_ONE * peselDigits[8] + Constants.WEIGHT_THREE * peselDigits[9];
        Integer moduloResult = computedChecksum % 10;

        if (moduloResult > 0) {
            computedChecksum = 10 - moduloResult;
        }

        return computedChecksum.equals(checksum) ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * Gets date of birth.
     *
     * @param pesel the pesel
     * @return the date of birth
     */
    public static Date getDateOfBirth(String pesel) {

        if (!isValid(pesel)) {
            throw new IllegalArgumentException(PESEL_IS_INVALID);
        }

        Calendar dobCal = Calendar.getInstance();
        dobCal.set(getYearOfBirth(pesel), getMonthOfBirth(pesel) - 1, getDayOfBirth(pesel));
        dobCal.set(Calendar.HOUR, 0);
        dobCal.set(Calendar.MINUTE, 0);
        dobCal.set(Calendar.SECOND, 0);
        return dobCal.getTime();
    }

    /**
     * Check if PESEL belongs to male.
     *
     * @param pesel the PESEL
     * @return true of PESEL belongs to male; false otherwise.
     */
    public static Boolean isMale(String pesel) {

        if (!isValid(pesel)) {
            throw new IllegalArgumentException(PESEL_IS_INVALID);
        }

        Character[] genderMaleArray = {'1', '3', '5', '7', '9'};
        Boolean male = Boolean.FALSE;

        Character gender = pesel.charAt(GENDER_CHAR_POSITION);
        for (Character val : genderMaleArray) {
            if (val == gender) {
                male = Boolean.TRUE;
                break;
            }
        }

        return male;
    }

    /**
     * Check if PESEL belongs to female.
     *
     * @param pesel the PESEL
     * @return true of PESEL belongs to male; false otherwise.
     */
    public static Boolean isFemale(String pesel) {
        return !isMale(pesel);
    }

    /**
     * Gets day of birth.
     *
     * @param pesel the PESEL
     * @return the day of birth
     */
    private static Integer getDayOfBirth(String pesel) {
        return Integer.parseInt(pesel.substring(4, 6));
    }

    /**
     * Gets month of birth.
     *
     * @param pesel the PESEL
     * @return the month of birth
     */
    private static Integer getMonthOfBirth(String pesel) {

        Integer monthOfBirth = Integer.parseInt(pesel.substring(2, 4));

        if ((monthOfBirth >= 21) && (monthOfBirth <= 32)) {
            monthOfBirth = monthOfBirth - 20;
        } else if ((monthOfBirth >= 41) && (monthOfBirth <= 52)) {
            monthOfBirth = monthOfBirth - 40;
        } else if ((monthOfBirth >= 61) && (monthOfBirth <= 72)) {
            monthOfBirth = monthOfBirth - 60;
        } else if ((monthOfBirth >= 81) && (monthOfBirth <= 92)) {
            monthOfBirth = monthOfBirth - 80;
        }

        return monthOfBirth;
    }

    /**
     * Gets year of birth.
     *
     * @param pesel the PESEL
     * @return the year of birth
     */
    private static Integer getYearOfBirth(String pesel) {

        Integer yearOfBirth = Integer.parseInt(pesel.substring(0, 2));
        Integer monthOfBirth = Integer.parseInt(pesel.substring(2, 4));

        if ((monthOfBirth >= 1) && (monthOfBirth <= 12)) {
            yearOfBirth = yearOfBirth + 1900;
        } else if ((monthOfBirth >= 21) && (monthOfBirth <= 32)) {
            yearOfBirth = yearOfBirth + 2000;
        } else if ((monthOfBirth >= 41) && (monthOfBirth <= 52)) {
            yearOfBirth = yearOfBirth + 2100;
        } else if ((monthOfBirth >= 61) && (monthOfBirth <= 72)) {
            yearOfBirth = yearOfBirth + 2200;
        } else if ((monthOfBirth >= 81) && (monthOfBirth <= 92)) {
            yearOfBirth = yearOfBirth + 1800;
        }

        return yearOfBirth;
    }

    /**
     * Check if PESEL has valid date.
     *
     * @param pesel the pesel
     * @return true if PESEL date is valid; false otherwise.
     */
    private static Boolean isDateValid(String pesel) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        sdf.setLenient(false);

        String formattedMonthOfBirth = String.format("%02d", getMonthOfBirth(pesel));
        String formattedDayOfBirth = String.format("%02d", getDayOfBirth(pesel));

        String dateToValidate = getYearOfBirth(pesel).toString() + formattedMonthOfBirth + formattedDayOfBirth;

        try {
            sdf.parse(dateToValidate);
        } catch (ParseException e) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }
}
