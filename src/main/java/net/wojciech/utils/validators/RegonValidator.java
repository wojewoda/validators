package net.wojciech.utils.validators;

/**
 * Polish REGON number validator.
 */
public class RegonValidator {

    /**
     * The constant REGON_LENGTH_LONG.
     */
    private static final int REGON_LENGTH_LONG = 14;
    /**
     * The constant REGON_LENGTH_SHORT.
     */
    private static final int REGON_LENGTH_SHORT = 9;

    /**
     * Utility class so prevent creating instances.
     */
    private RegonValidator() {
        // empty
    }

    /**
     * Check if REGON number is valid.
     *
     * @param regon the REGON number
     * @return true if REGON is valid; false otherwise.
     */
    public static Boolean isValid(String regon) {

        if (regon == null || (regon.length() != REGON_LENGTH_SHORT) && (regon.length() != REGON_LENGTH_LONG)) {
            return Boolean.FALSE;
        }

        Integer[] regonDigits = new Integer[REGON_LENGTH_LONG];

        int i = 0;
        for (Character element : regon.toCharArray()) {
            if (!Character.isDigit(element)) {
                return Boolean.FALSE;
            } else {
                regonDigits[i] = Integer.parseInt(element.toString());
                i++;
            }
        }

        Integer checksum = Integer.parseInt(regon.substring(regon.length() - 1));
        Integer computedChecksum = 999;

        if (regon.length() == REGON_LENGTH_SHORT) {
            computedChecksum = Constants.WEIGHT_EIGHT * regonDigits[0] + Constants.WEIGHT_NINE * regonDigits[1]
                    + Constants.WEIGHT_TWO * regonDigits[2] + Constants.WEIGHT_THREE * regonDigits[3]
                    + Constants.WEIGHT_FOUR * regonDigits[4] + Constants.WEIGHT_FIVE * regonDigits[5]
                    + Constants.WEIGHT_SIX * regonDigits[6] + Constants.WEIGHT_SEVEN * regonDigits[7];
        } else if (regon.length() == REGON_LENGTH_LONG) {
            computedChecksum = Constants.WEIGHT_TWO * regonDigits[0] + Constants.WEIGHT_FOUR * regonDigits[1]
                    + Constants.WEIGHT_EIGHT * regonDigits[2] + Constants.WEIGHT_FIVE * regonDigits[3]
                    + Constants.WEIGHT_ZERO * regonDigits[4] + Constants.WEIGHT_NINE * regonDigits[5]
                    + Constants.WEIGHT_SEVEN * regonDigits[6] + Constants.WEIGHT_THREE * regonDigits[7]
                    + Constants.WEIGHT_SIX * regonDigits[8] + Constants.WEIGHT_ONE * regonDigits[9] + Constants.WEIGHT_TWO * regonDigits[10]
                    + Constants.WEIGHT_FOUR * regonDigits[11] + Constants.WEIGHT_EIGHT * regonDigits[12];
        }

        computedChecksum = computedChecksum % 11;

        if (computedChecksum == 10) {
            computedChecksum = 0;
        }

        return computedChecksum.equals(checksum) ? Boolean.TRUE : Boolean.FALSE;
    }
}
