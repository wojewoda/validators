package net.wojciech.utils.validators.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import net.wojciech.utils.validators.RegonValidator;

import org.junit.Test;

/**
 * The RegonValidator test.
 */
public class RegonValidatorTest {

    /**
     * The constant VALID_REGON_SHORT.
     */
    private static final String VALID_REGON_SHORT = "210191799";

    /**
     * The constant VALID_REGON_LONG.
     */
    private static final String VALID_REGON_LONG = "49699794813738";

    /**
     * The constant INVALID_REGON_SHORT.
     */
    private static final String INVALID_REGON_SHORT = "210491799";

    /**
     * The constant INVALID_REGON_LONG.
     */
    private static final String INVALID_REGON_LONG = "49699704813738";

    /**
     * Test is valid - for short REGON
     */
    @Test
    public void testIsValidShort() {
        assertTrue(RegonValidator.isValid(VALID_REGON_SHORT));
    }

    /**
     * Test is invalid - for short REGON
     */
    @Test
    public void testIsInvalidShort() {
        assertFalse(RegonValidator.isValid(INVALID_REGON_SHORT));
    }

    /**
     * Test is valid - for long REGON
     */
    @Test
    public void testIsValidLong() {
        assertTrue(RegonValidator.isValid(VALID_REGON_LONG));
    }

    /**
     * Test is invalid - for long REGON
     */
    @Test
    public void testIsInvalidLong() {
        assertFalse(RegonValidator.isValid(INVALID_REGON_LONG));
    }
}
